# HMIN339 : Méthodes Avancées de la Science de données - Reconnaissance visuelle d'images de fleurs

### Objectif du Project :
Reconnaissance d'espèces de plantes à partir de photos

### Jeu de Départ :
3474 images appartenant à 50 espèces différentes

### Encadrement :
* **`Konstantin TODOROV`**
* **`Pascal PONCELET`**

### Fait par :
* **`BEYA NTUMBA Joel`**
* **`MINKO AMOA Dareine`**
* **`QUENETTE Christophe`**
* **`SHAQURA Tasnim`**
